# Selection Structures - Password Strength

![main1.png](images/main1.png)
![main2.png](images/main2.png)
![main3.png](images/main3.png)


In almost every instance in which we want to solve a problem, we choose between different options depending on whether certain conditions are met. Computer programs are built to solve problems, therefore they should have a structure that allows them to make decisions and select alternatives. In C++, selections are structured using `if`, `else`, `else if` or `switch`. Relational expressions and logical operators are common when handling selection structures. In this laboratory experience, you will practice the use of some of these selection structures by completing the design of an application that determines the strength of a password.

## Objectives:

1. Use relational expressions and select adequate logical operators to make decisions.
2. Apply selection structures.


## Pre-Lab:

Before you get to the laboratory you should have:

1. Reviewed the following concepts:

    a. logical operators.

    b. `if`, `else`, `else if`.

2. Reviewed the use of objects of the `string` class and its `length()` method.

3. Studied the concepts and instructions for this laboratory session.

4. Taken the Pre-Lab quiz, available in Moodle.

---

---

## Password Strength

Using strong passwords is essential to securing information. A password is considered strong if it is not cost-effective for a hacker to try and guess it using multiple methods or brute force. For example, a password that consists of a simple dictionary word, without digits, symbols or uppercase letters, is so easy to decipher that even a caveman could do it.

Since an official system to measure password strength doesn't exist, we will use formulas created by the passwordmeter to evaluate the general strength of a given password [1]. We recommend that you play around a bit with the application in http://passwordmeter.com so that you understand how the application you will be implementing should behave. The strength of the password will be quantified by adding points for using good techniques of password selection (like using symbols and letters), and subtracting points for using bad habits (like only using lowercase letters or consecutive symbols of the same type).

The following tables review the added and subtracted values for various criteria in passwords.


### Assigning Points to a Password

#### Adding Points:

---



|      | Category                        | Points                                   | Notes                                                |
| :--- | :-------------------------------- | :-------------------------------------: | ----------------------------------------------------: |
| 1.   | Number of characters             | $$4\left(len\right)$$                                 | $$len$$ is the length of the password                        |
| 2.   | Uppercase letters                | ![addUpper.png](images/small/addUpper.png) | $$n$$ is the number of uppercase letters                |
| 3.   | Lowercase letters                | ![addLower.png](images/small/addLower.png)  | $$n$$ is the number of lowercase letters                |
| 4.   | Digits                          |  ![addDigits.png](images/small/addDigits.png) | $$n$$ is the number of digits                          |
| 5.   | Symbols                        | $$6n$$     | $$n$$ is the number of symbols                       |
| 6.   | Digits or symbols in the middle   | $$2n$$                                    | $$n$$ is the number of digits or symbols in the middle   |
| 7.   | Requisites                       | ![criteria.png](images/small/criteria.png)                              | $$n$$ is the number of criteria that are met        |

**Table 1.** Positive criteria for password strength.

---

What follows are some additional details and examples for the criteria of **adding points**.

1. **Number of Characters:** this is the simplest criteria. The score will be $$4$$ times the length of the password. For example, `"ab453"` has a count of $$5$$ and a score of $$4 \cdot 5 = 20$$.

2. **Uppercase Letters:** the score is $$2 \left(len - n \right)$$ if the password consists of a mix of uppercase letters **and** at least another type of character (lowercase, digits, symbols). If not, the score is $$0$$. For example:

    a. The score for `"ab453"` would be $$0$$ since it doesn't have uppercase letters (the count is also $$0$$).

    b. The score for `"ALGO"` would be $$0$$ since it **only** has uppercase letters (the count is $$4$$).

    c. The score for `"SANC8in"` would be $$2  \left(7-4\right) = 6$$ since the password has a length of $$7$$, has $$4$$ uppercase letters, and contains characters of another type (the count is $$4$$).

3. **Lowercase Letters:** the score is $$2 \left(len - n\right)$$ if the password is a mix of lowercase letters **and** at least another type of character (uppercase, digits, symbols). If not, the score is $$0$$. For example:

    a. The score for `"ab453"` would be $$2 \left(5-2\right) = 6$$ because the password has a length of$$5$$, has $$2$$ lowercase letters, and contains characters of another type. The count is $$2$$.

    b. The score for `"ALGO"` would be $$0$$ because it doesn't have lowercase letters. The count is $$0$$.

    c. The score for `"sancochin"`  would be $$0$$ because it **only** has lowercase letters. The count is $$9$$.

4. **Digits:** the score is $$4n$$ if the password consists of a mix of digits **and** at least another type of character (lowercase, uppercase, symbols). If not, the score is $$0$$. For example:

    a. The score for `"ab453"` would be  $$4 \cdot 3 = 12$$ because the password contains $$3$$ digits and contains characters of another type.

    b. The score for `"ALGO"` would be $$0$$ because it doesn't have digits.

    c. The score for `801145555` would be $$0$$ because it **only** has digits.

5. **Symbols:** the score is $$6n$$ if the password contains $$n$$ symbols. Otherwise, the score is $$0$$. For example:

    a. The score for `"ab453"` would be $$0$$ because it does not contain symbols.

    b. The score for `"ALGO!!"` would be $$6 \cdot 2 = 12$$ because it contains $$2$$ symbols and contains other types of characters.

    c. The score for `"---><&&"` would be $$6 \cdot 7 = 42$$ because it contains $$7$$ symbols. Note that in the case of symbols, points are given even when there aren't other types of characters.

6. **Digits or Symbols in the Middle:** the score is $$2n$$ if the password has symbols or digits that are not in the first or last position. For example:

    a. The score for `"ab453"` would be $$2 \cdot2 = 4$$ because it has 2 digits that are not in the first or last position, these are `4` and `5`.

    b. The score for `"ALGO!"` would be $$0$$ because it does not contain digits or symbols in the middle, the only symbol is in the last position.

    c. The score for `S&c8i7o!` would be $$2 \cdot 3 = 6$$ because it has $$3$$ symbols or digits in the middle, these are `&`, 8`, and `7`.

7. **Requisites:** the score is $$2n$$ only if the length criteria **and** 3 or 4 of the other criteria are met, where $$n$$ is the number of *criteria* that is met. The criteria are:

    a. The password must have 8 or more characters of length.

    b. The password must have:
    
       - Uppercase letters
          
       - Lowercase letters
          
       - Numbers
          
       - Symbols

       Each of the items listed in part b. count as one individual criteria. For example:

       i. The score for `"ab453"` would be $$0$$ because the criteria for length is not met.

       ii. The score for `"abABCDEF"` would be $$0$$ because, despite the fact that the length criteria is met, only 2 of the 4 other criteria are met (uppercase and lowercase letters).

       iii. The score for `"abAB99!!"` would be $$2 \cdot 5 = 10$$ because the length criteria and the other 4 criteria are met.


#### Subtracting Points

---

|      | Category                         | Points                                   | Notes                                                                  |
| :--- | :-------------------------------- | :-------------------------------------: | ----------------------------------------------------:                   |
| 1.   | Only letters                       |![subsLetters.png](images/small/subsLetters.png)    | $$len$$ is the length of the password                                          |
| 2.   | Only digits                      | ![subsDigits.png](images/small/subsDigits.png)    | $$len$$ is the length of the password                                           |
| 3.   | Consecutive uppercase letters    | $$-2n$$                                   | $$n$$ is the number of uppercase letters that follow another uppercase letter |
| 4.   | Consecutive lowercase letters   | $$-2n$$                                   | $$n$$ is the number of lowercase letters that follow another lowercase letter                                                                     |
| 5.   | Consecutive digits             | $$-2n$$                                   | $$n$$ is the number of digits that follow another digit                     |


**Table 2.** Negative criteria for password strength.

---

The following are additional details and examples of the criteria for **subtracting points**.

1. **Letters Only:** the score is $$-len$$ for a password that consists of letters only, otherwise it is $$0$$. For example:

      a. The score for `"ab453"` would be $$0$$ since it contains letters and numbers.

      b. The score for `"Barrunto"` would be $$-8$$ since it only contains letters and its length is $$8$$.

2. **Digits Only:** the score is $$-len$$ for a password that consists of digits only, otherwise it is $$0$$. For example:

      a. The score for `"ab453"` would be $$0$$ since it contains only letters and numbers.

      b. The score for `"987987987"` would be $$-9$$ since it contains only digits and its length is $$9$$.

3. **Consecutive Uppercase Letters:** the score is $$-2n$$ where $$n$$ is the number of uppercase letters that follow another uppercase letter. For example,

      a. the score for `"DB453"` would be $$-2 \cdot 1 = -2$$ since it only contains one uppercase letter (`B`) that follows another uppercase letter.

      b. the score for `"TNS1PBMA"` would be $$-2 \cdot 5 = -10$$ since it contains 5 uppercase letters (`N`, `S`, `B`, `M`, `A`) that follow another uppercase letter.

4. **Consecutive Lowercase Letters:** the same as for criteria #3, but for lowercase letters.

5. **Consecutive Digits:** the same as for criteria #3, but for digits.


---

---

!INCLUDE "../../eip-diagnostic/password-strength/en/diag-password-strength-01.html"
<br>

!INCLUDE "../../eip-diagnostic/password-strength/en/diag-password-strength-02.html"
<br>

!INCLUDE "../../eip-diagnostic/password-strength/en/diag-password-strength-03.html"
<br>

!INCLUDE "../../eip-diagnostic/password-strength/en/diag-password-strength-04.html"
<br>

!INCLUDE "../../eip-diagnostic/password-strength/en/diag-password-strength-05.html"
<br>

---

---

## Laboratory Session:

In this laboratory experience you will practice the use of mathematical expressions and selection structures to compute the score for the strength of a password combining the points for the individual criteria.

Your task is to complete the design of the application to measure the strength of a password. When done, you will obtain a simplified version of the application in http://www.passwordmeter.com. Since there isn't an official system to measure passwords, the formulas created by "passwordmeter" will be used to evaluate the general strength of a given password. The application will allow users to enter a password and calculate its strength using a series of rules.

The strength of the password will be quantified by adding points for using good password selection techniques (like combining symbols and letters) and subtracting points for using bad habits (like using only uppercase letters or consecutive symbols of the same type). Your program will analyze the password given by the user and use the criteria in the tables presented above to compute a score for the password's strength.

Once the application is complete, it will show a window where, as the password characters are entered, the partial score will be displayed. This graphical interface will offer the user a way to improve his password and correct typical weak password habits.




### Exercise 1 - Familiarize Yourself with the Pre-defined Functions

The first step in this laboratory experience is to familiarize yourself with the functions that are pre-defined in the code.  You will call these functions as part of your own code to compute the score of the various password strength criteria.

#### Instructions

1. Load the project  `PasswordStrength` into `QtCreator`. There are two ways to do this:

    * Using the virtual machine: Double click the file `PassworStrength.pro` located in the folder `/home/eip/labs/selections-passwordstrength` of your virtual machine.
    * Downloading the project’s folder from `Bitbucket`: Use a terminal and write the command `git clone http:/bitbucket.org/eip-uprrp/selections-passwordstrength` to download the folder `selections-passwordstrength` from `Bitbucket`. Double click the file `PassworStrength.pro` located in the folder that you downloaded to your computer.

2. Configure the project. The project consists of several files. **You will only write code in the file  `readpassword.cpp`. You should not make any changes in the other files.** Despite this, you should familiarize yourself with the functions that are already defined in them, since you will be using some of them to create your code.

  * `psfunctions.cpp` : contains the implementations of some of the functions that will invoke in your program to calculate the score for the password's strength. **You do not have to change anything in this file or in the file `psfunctions.h`**. Simply invoke the functions as necessary from the `readPass` function in the `readpassword.cpp` file. Note that the function names tell you what the functions do.

  * `psfunctions.h` : contains the prototypes for the functions defined in `psfunctions.cpp`.


### Exercise 2 - Understand the Functions to Update the User's Graphical Interface.

In the laboratory exercise you will write code to calculate the score associated to each one of the criteria in the tables for adding and subtracting points shown above. These scores should be updated in the user's graphical interface that is shown in Figure 1.

---

![interfaceGrafica.png](images/interfaceGrafica.png)

**Figure 1.** User graphical interface for *Password strength* project.

---

There are predefined functions that update the graphical interface. For the application to work properly, each time that your code computes the score that is given for each criteria you should invoke the function to update that particular criteria in the graphical interface. The functions to update the criteria have the following syntax:


```
void setCRITERIA(int count, int score) ;
```

where CRITERIA should be replaced by the criteria that is being evaluated. Observe that the function requires two arguments: the **count**, that is the amount of characters that meet the criteria, and the **score**, that is the calculation that you will implement following the tables presented above. For example:

```
count = pass.length() ;
score = 4 * count ;
setNumberOfCharacters(count, score);
totalScore += score ;
```

  In the above code `count` contains the number of characters in the password, `score` contains the computation for the score of the criteria for the number of characters, and `setNumberOfCharacters(count, score);` invokes the function to update the corresponding information for the criteria "Number of characters" in the graphical interface.

  The functions to update the graphical interface are:
  
```
// To update the password's length.
void setNumberOfCharacters(int count, int score) ;  

// For adding points

// To update the uppercase characters.
void setUpperCharacters(int count, int score) ;

// To update the lowercase characters.
void setLowerCharacters(int count, int score) ;

// To update the characters that are digits.
void setDigits(int count, int score) ;

// To update the characters that are symbols.
void setSymbols(int count, int score) ;

// To update the digits or symbols in the middle
void setMiddleDigitsOrSymbols(int count, int score) ;

// To update the criterium of the requisites
void setRequirements(int count, int score) ;

// For subtracting points

// To update the criterium of only letters.
void setLettersOnly(int count, int score) ;

// To update the criterium of only digits.
void setDigitsOnly(int count, int score) ;

// To update the criterium of consecutive uppercase letters.
void setConsecutiveUpper(int count, int score) ;

// To update the criterium of consecutive lowercase letters.
void setConsecutiveLower(int count, int score) ;

// To update the criterium of consecutive digits.
void setConsecutiveDigits(int count, int score) ;
```


### Exercise 3 - Compute the Score for the Criteria and the Total Score for the Password

The code that we're providing you contains the functions that compute the count for the majority of the criteria and whose names reflect what they do and what the function returns. For example, `countUppercase`, return the number of characters that are uppercase letters.

You can find a list and description of the functions in [this link](doc/en/html/index.html).

Your task is to use mathematical expressions and decision structures for the individual criteria scores and combine them to compute the total score for the password's strength.

**Example 1:**

---

![ventanaCaba77o.png](images/ventanaCaba77o.png)

**Figure 2.** Window with the report for the password `caba77o`.

---



**Example 2:**

---

![ventanaS1nf@nia!.png](images/ventanaS1nf@nia!.png)

**Figure 3.** Window with the report for the password `S1nf@nia!`.

---

In Example 2, the number of **requisites** is 5 because `"S1nf@nia!"` meets the criteria for length and also contains uppercase letters, lowercase letters, numbers and symbols. Therefore, the score for the number of requisites is $$2 \cdot 5 =10$$.

In the project's code you will find examples of how to calculate the first two positive criteria: the number of characters in the password and the numbers of uppercase letters. You can compile and execute the example so you can see the working interface with these two criteria. Part of your task is to add the code to compute the score for the remaining criteria. Remember that you should accumulate the total score and invoke the functions to update the graphical interface.


### Exercise 4 - Determine and Display the Password's Strength

The user will input the password in the top section of the graphical interface. Below appears the *report* that contains the different criteria, the count for each criteria, and the individual score for the criteria. This report will be updated as the user inputs the password's characters. The total score will be the sum of all of the points (addition and subtraction) of the individual criteria.

Based on the total score, the program will classify the password's strength as follows:

|   Total score   |  Strength   |
|-----------------|-------------|
| [0,20)          | Very weak   |
| [20,40)         | Weak        |
| [40,60)         | Good        |
| [60,80)         | Strong      |
| [80,100]        | Very strong |


The provided code already invokes the `strengthDisplay` function with the strength calculated and the total score to update the classification, and the bar that indicates the password's strength in the graphical interface.

---

---

## Deliverables

Use "Deliverable" in Moodle to upload the `readpassword.cpp` file that contains the code with the computation for the score of the individual criteria, the final score, the function calls to update the graphical interface, the password's classification and the displayed functions. Remember to use good programming techniques, include the name of the programmers involved, and document your program.



---

---

## References

[1] Passwordmeter, http://www.passwordmeter.com/

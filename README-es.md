# Estructuras de selección - Fortaleza de contraseñas

![main1.png](images/main1.png)
![main2.png](images/main2.png)
![main3.png](images/main3.png)


En casi todas las instancias en las cuales queremos resolver un problema seleccionamos una o más opciones dependiendo de si se cumplen o no ciertas condiciones. Los programas de computadoras se construyen para resolver problemas y, por lo tanto, deben tener una estructura que permita tomar decisiones y seleccionar alternativas. En C++ las selecciones se estructuran utilizando `if`, `else`, `else if` o `switch`. Muchas veces el uso de estas estructuras también envuelve el uso de expresiones de relación y operadores lógicos. En la experiencia de laboratorio de hoy, practicarás el uso de algunas estructuras de selección, completando el diseño de una aplicación que determina la fortaleza de una contraseña de acceso ("password").

## Objetivos:

1. Utilizar expresiones relacionales y seleccionar operadores lógicos adecuados para la toma de decisiones.
2. Aplicar estructuras de selección.


## Pre-Lab:

Antes de llegar al laboratorio debes:

1. Haber repasado los siguientes conceptos:

    a. operadores lógicos

    b. `if`, `else`, `else if`.

2. Haber repasado el uso de objetos de la clase `string` y su método `length()`.

3. Haber estudiado los conceptos e instrucciones para la sesión de laboratorio.

4. Haber tomado el quiz Pre-Lab disponible en Moodle.


---

---

## Fortaleza de contraseñas de acceso

Utilizar contraseñas de acceso resistentes es esencial para mantener los datos seguros en los sistemas de información. Una contraseña se considera resistente o fuerte si resulta que no es costo-efectivo para el "pirata informático" ("hacker") emplear tiempo tratando de adivinarla usando contraseñas ingenuas o fuerza bruta. Por ejemplo, una contraseña que consiste de una palabra simple del diccionario, sin números, símbolos o letras mayúsculas, es tan fácil de descifrar que hasta "un cavernícola puede hacerlo".

Como no existe un sistema oficial para medir las contraseñas, utilizaremos fórmulas creadas por el "passwordmeter"  para evaluar la fortaleza general de una contraseña dada [1]. Te recomendamos que juegues un poco con la aplicación en http://passwordmeter.com para que entiendas cómo debe comportarse la aplicación que estarás implementando en esta experiencia de laboratorio. La fortaleza de la contraseña se cuantificará otorgando puntos por utilizar buenas técnicas de selección de contraseñas (como utilizar mezclas de símbolos y letras) y restando puntos por utilizar malos hábitos en las contraseñas (como utilizar solo letras minúsculas o símbolos consecutivos de un mismo tipo).

Las siguientes tablas resumen los valores añadidos y sustraídos para varias características en las contraseñas.



### Asignando puntuación a las contraseñas

#### Sumando puntos

---

|      | Categoría                        | Puntos                                   | Notas                                                |
| :--- | :-------------------------------- | :-------------------------------------: | ----------------------------------------------------: |
| 1.   | Número de caracteres             | $$4\left(len\right)$$                                 | $$len$$ es el largo de la contraseña                        |
| 2.   | Letras mayúsculas                | ![sumaMayus.png](images/small/sumaMayus.png)| $$n$$ es el número de letras mayúsculas                |
| 3.   | Letras minúsculas                | ![sumaMin.png](images/small/sumaMin.png) | $$n$$ es el número de letras minúsculas                |
| 4.   | Dígitos                          |  ![sumaDigitos.png](images/small/sumaDigitos.png) | $$n$$ es el número de dígitos                          |
| 5.   | Símbolos                         | $$6n$$     | $$n$$ es el número de símbolos                         |
| 6.   | Dígitos o símbolos en el medio   | $$2n$$                                   | $$n$$ es el número de dígitos y símbolos en el medio   |
| 7.   | Requisitos                       | ![criterios.png](images/small/criterios.png)                              | $$n$$ es el número de criterios que se cumplen        |

**Tabla 1.** Criterios positivos para la fortaleza de la contraseña.

---

Lo que sigue son algunos detalles adicionales y ejemplos de los criterios para **sumar puntos**.

1. **Número de caracteres:** este es el criterio más simple. La puntuación es $$4$$ veces el largo de la contraseña. Por ejemplo, `"ab453"` tiene un conteo de $$5$$ y puntuación de $$4 \cdot 5 = 20$$.

2. **Letras mayúsculas:** la puntuación es $$2 \left(len - n \right)$$ si la contraseña consiste de una mezcla de letras mayúsculas **y** al menos otro tipo de caracter (minúscula, dígitos, símbolos). De lo contrario, la puntuación es $$0$$. Por ejemplo:

    a. La puntuación para `"ab453"` sería $$0$$ ya que no tiene letras mayúsculas (el conteo también es $$0$$).

    b. La puntuación para `"ALGO"` sería $$0$$ porque **solo** contiene letras mayúsculas (el conteo es $$4$$).

    c. La puntuación para `"SANC8in"` sería $$2  \left(7-4\right) = 6$$ porque la contraseña es de largo $$7$$, contiene $$4$$ letras mayúsculas, y contiene caracteres de otro tipo (el conteo es $$4$$).

3. **Letras minúsculas:** La puntuación es $$2 \left(len - n\right)$$ si la contraseña es una mezcla de letras minúsculas **y** al menos otro tipo de caracter (mayúscula, dígitos, símbolos). De lo contrario, la puntuación es $$0$$. Por ejemplo:

    a. La puntuación para `"ab453"` sería $$2 \left(5-2\right) = 6$$ porque la contraseña es de largo $$5$$, contiene $$2$$ letras minúsculas, y contiene caracteres de otro tipo. El conteo es $$2$$.

    b. La puntuación para `"ALGO"` sería $$0$$ porque no contiene letras minúsculas. El conteo es $$0$$.

    c. La puntuación para `"sancochin"`  sería $$0$$ porque contiene **solo** letras minúsculas. El conteo es $$9$$.

4. **Dígitos:** la puntuación es $$4n$$ si la contraseña consiste de una mezcla de dígitos **y** al menos otro tipo de caracter (minúscula, mayúscula, símbolos). De otro modo la puntuación es $$0$$. Por ejemplo:

      a. La puntuación para `"ab453"` sería $$4 \cdot3 = 12$$ porque la contraseña contiene $$3$$ dígitos y contiene caracteres de otro tipo.

      b. La puntuación para `"ALGO"` sería $$0$$ porque no tiene dígitos.

      c. La puntuación para `801145555` sería $$0$$ porque contiene **solo** dígitos.

5. **Símbolos:** la puntuación es $$6n$$ si la contraseña contiene $$n$$ símbolos. De otro modo la puntuación es $$0$$. Por ejemplo:

      a. La puntuación para `"ab453"` sería $$0$$ porque no contiene símbolos.

      b. La puntuación para `"ALGO!!"` sería $$6 \cdot 2 = 12$$ porque contiene $$2$$ símbolos y contiene otros tipos de caracteres.

      c. La puntuación para `"---><&&"`  sería $$6 \cdot 7 = 42$$ porque contiene $$7$$ símbolos. Nota que en el caso de símbolos, se otorga puntuación incluso cuando no hay otro tipo de caracteres.

6. **Dígitos o símbolos en el medio:** la puntuación es $$2n$$ si la contraseña contiene símbolos o dígitos que no están en la primera o última posición. Por ejemplo:

      a. La puntuación para `"ab453"` sería $$2 \cdot2 = 4$$ porque contiene dos dígitos que no están en la primera o última posición, estos son `4` y `5`.

      b. La puntuación para `"ALGO!"` sería $$0$$ porque no contiene dígitos ni símbolos en el medio, el único símbolo está al final.

      c. La puntuación para `S&c8i7o!`  sería $$2 \cdot 3 = 6$$ porque contiene $$3$$ símbolos o dígitos en el medio, estos son `&`, 8`, y `7`.

7. **Requisitos:** se otorga  $$2n$$ solo si el criterio del largo **y** 3 o 4 de los otros criterios se cumplen, donde $$n$$ es el número de *criterios* que se cumplen. Los criterios son:

    a. La contraseña debe tener 8 o más caracteres de largo.

    b. Contener:

       - Letras mayúsculas
        
       - Letras minúsculas
        
       - Números
        
       - Símbolos

       Cada uno de los listados en la parte b. cuenta como un criterio individual. Por ejemplo:

          i. La puntuación para `"ab453"` sería $$0$$ porque el criterio del largo no se cumple.

          ii. La puntuación para `"abABCDEF"` sería $$0$$ debido a que, a pesar de que se cumple el criterio del largo, solo 2 de los 4 otros criterios se cumplen (mayúsculas y minúsculas).

          iii. La puntuación para `"abAB99!!"` sería $$2 \cdot 5 = 10$$ debido a que cumple la condición del largo y también los otros 4 criterios.


#### Restando puntos

---

|      | Categoría                         | Puntos                                   | Notas                                                                   |
| :--- | :-------------------------------- | :-------------------------------------: | ----------------------------------------------------:                   |
| 1.   | Solo letras                       |![resLetras.png](images/small/resLetras.png)    | $$len$$ es el largo de la contraseña                                           |
| 2.   | Solo dígitos                      | ![resDigitos.png](images/small/resDigitos.png)    | $$len$$ es el largo de la contraseña                                           |
| 3.   | Letras mayúsculas consecutivas    | $$-2n$$                                   | $$n$$ es el número de letras mayúsculas que siguen a otra letra mayúscula |
| 4.   | Letras minúsculas consecutivas    | $$-2n$$                                   | $$n$$ es el número de letras minúsculas que siguen a otra letra minúscula                                                                     |
| 5.   | Dígitos consecutivos              | $$-2n$$                                   | $$n$$ es el número de dígitos que siguen a otro dígito                     |

**Tabla 2.** Criterios negativos para la fortaleza de la contraseña.

---

Lo que sigue son algunos detalles adicionales y ejemplos de los criterios para **restar puntos**.

1. **Letras solamente**: La puntuación es $$-len$$ para una contraseña que consista solo de letras, de otro modo obtiene $$0$$. Por ejemplo:

    a. La puntuación para `"ab453"` sería $$0$$ ya que contiene letras y números.

    b. La puntuación para `"Barrunto"` sería $$-8$$ ya que consiste solo de letras y su largo es $$8$$.

2. **Dígitos solamente**: La puntuación es $$-len$$ para una contraseña que consista solo de dígitos, de otro modo obtiene $$0$$. Por ejemplo:

    a. La puntuación para `"ab453"` sería $$0$$ ya que contiene solo letras y números.

    b. La puntuación para `"987987987"` sería $$-9$$ ya que consiste solo de dígitos y su largo es $$9$$.

3. **Letras mayúsculas consecutivas**: La puntuación es $$-2n$$ donde $$n$$ es el número de letras mayúsculas que siguen a otra letra mayúscula. Por ejemplo:

    a. La puntuación para `"DB453"` sería $$-2 \cdot 1 = -2$$ ya que solo contiene una letra mayúscula (`B`) que sigue a otra letra mayúscula.

    b. La puntuación para `"TNS1PBMA"` sería $$-2 \cdot 5 = -10$$ ya que contiene 5 letras mayúsculas (`N`, `S`, `B`, `M`, `A`) que siguen a otra letra mayúscula.

4. **Letras minúsculas consecutivas**: Igual que el criterio #3, pero para letras minúsculas.

5. **Dígitos consecutivos**: Igual que el criterio #3, pero para dígitos.

---

---

!INCLUDE "../../eip-diagnostic/password-strength/es/diag-password-strength-01.html"
<br>

!INCLUDE "../../eip-diagnostic/password-strength/es/diag-password-strength-02.html"
<br>

!INCLUDE "../../eip-diagnostic/password-strength/es/diag-password-strength-03.html"
<br>

!INCLUDE "../../eip-diagnostic/password-strength/es/diag-password-strength-04.html"
<br>

!INCLUDE "../../eip-diagnostic/password-strength/es/diag-password-strength-05.html"
<br>

---

---

## Sesión de laboratorio:

En esta experiencia de laboratorio practicarás el uso de expresiones matemáticas y estructuras de selección para computar la puntuación de resistencia o fortaleza de una contraseña, combinando las puntuaciones de los criterios individuales.

Tu tarea es completar el diseño de una aplicación para medir la fortaleza de las contraseñas de acceso. Al final, obtendrás un programa que será una versión simplificada de la aplicación en http://www.passwordmeter.com . Como no existe un sistema oficial para medir las contraseñas, se utilizarán las fórmulas creadas por el "passwordmeter" para evaluar la fortaleza general de una contraseña dada. La aplicación permitirá al usuario entrar una contraseña y calculará su fortaleza utilizando una serie de reglas.

La fortaleza de la contraseña se cuantificará otorgando puntos por utilizar buenas técnicas de selección de contraseñas (como combinar símbolos y letras) y restando puntos por utilizar malos hábitos (como utilizar solo letras minúsculas o caracteres consecutivos de un mismo tipo). Tu programa analizará la contraseña dada por el usuario, y usará los criterios en las tablas presentadas arriba para computar una puntuación para la fortaleza de la contraseña.

Una vez completada la aplicación, esta mostrará una ventana en donde, según se vayan entrando los caracteres de la contraseña, se desglosará la puntuación parcial obtenida. Esta interfaz gráfica para el usuario le ofrecerá una manera de mejorar su contraseña y corregir los malos hábitos típicos al formular contraseñas débiles.



### Ejercicio 1 - Familiarizarte con las funciones pre-definidas

El primer paso en esta experiencia de laboratorio es familiarizarte con las funciones pre-definidas en el código. Invocarás estas funciones en el código que crearás para computar la puntuación de varios de los criterios para fortaleza de contraseñas.


#### Instrucciones

1.  Carga a `QtCreator` el proyecto `PassworStrength`. Hay dos maneras de hacer esto:

    * Utilizando la máquina virtual: Haz doble “click” en el archivo `PassworStrength.pro` que se encuentra  en el directorio `/home/eip/labs/selections-passwordstrength` de la máquina virtual.
    * Descargando la carpeta del proyecto de `Bitbucket`: Utiliza un terminal y escribe el commando `git clone http:/bitbucket.org/eip-uprrp/selections-passwordstrength` para descargar la carpeta `selections-passwordstrength` de `Bitbucket`. En esa carpeta, haz doble “click” en el archivo `PassworStrength.pro`.

2. Configura el proyecto.  El proyecto consiste de varios archivos. **Solo escribirás código en el archivo `readpassword.cpp`. No debes cambiar nada en los demás archivos.** Sin embargo, debes familiarizarte con las funciones que ya están definidas en ellos, ya que usarás algunas de ellas para crear tu código.

      * `psfunctions.cpp` : contiene las implementaciones de algunas de las funciones que tu programa va a invocar para calcular la puntuación de la fortaleza total de la contraseña. **No tienes que cambiar nada del código en este archivo ni en el archivo `psfunctions.h`**. Simplemente invocarás desde la función `readPass` en el archivo `readpassword.cpp` las funciones contenidas en ellos, según sea necesario. Hay funciones que no necesitarás invocar. Nota que el nombre de las funciones te dice lo que la función hace.

      * `psfunctions.h` : contiene los prototipos de las funciones definidas en `psfunctions.cpp`.



### Ejercicio 2 - Conocer las funciones para actualizar la interfaz gráfica de usuarios.

En el ejercicio de laboratorio crearás el código para calcular la puntuación asociada a cada uno de los criterios de las tablas de sumas y deducciones mostradas arriba. Estas puntuaciones deben ser actualizadas en la interfaz gráfica de usuarios que se muestra en la Figura 1.

---

![interfaceGrafica.png](images/interfaceGrafica.png)

**Figura 1.** Interfaz gráfica de usuarios del proyecto *Fortaleza de contraseñas*

---

Hay funciones pre-definidas que actualizan la interfaz gráfica. Para que la aplicación funcione como esperada, cada vez que tu código compute la puntuación que se adjudica para un criterio debes invocar la función que actualiza ese criterio en el interfaz gráfico. Las funciones para actualizar los criterios tienen la siguiente sintaxis:  


  ```
  void setCRITERIO(int count, int score) ;
  ```

  donde CRITERIO debe reemplazarse por el criterio evaluado. Observa que la función requiere dos argumentos: el **conteo**, que es la cantidad de caracteres que cumple con el criterio, y la **puntuación**, que es el cálculo que tú implementarás siguiendo las tablas de arriba.  Por ejemplo:

```
count = pass.length() ;
score = 4 * count ;
setNumberOfCharacters(count, score);
totalScore += score ;

```
En  el código de arriba `count` contiene el número de caracteres en la contraseña, `score` contiene el cómputo de la puntuación del criterio de número de caracteres, y `setNumberOfCharacters(count, score);` invoca la función para que se actualice la información correspondiente al criterio “Number of characters” en la interfaz gráfica.


  Las funciones para actualizar la interfaz gráfica son:
  
```
// Para actualizar el largo de  la contraseña.
void setNumberOfCharacters(int count, int score) ;  

// Para las sumas

// Para actualizar los caracteres en mayúscula.
void setUpperCharacters(int count, int score) ;

// Para actualizar los caracteres en minúscula.
void setLowerCharacters(int count, int score) ;

// Para actualizar los caracteres que son dígitos.
void setDigits(int count, int score) ;

// Para actualizar los caracteres que son símbolos.
void setSymbols(int count, int score) ;

// Para actualizar digitos o simbolos en el medio
void setMiddleDigitsOrSymbols(int count, int score) ;

// Para actualizar el criterio de  los requisitos
void setRequirements(int count, int score) ;

// Para las restas

// Para actualizar el criterio de letras solamente.
void setLettersOnly(int count, int score) ;

// Para actualizar el criterio de dígitos solamente.
void setDigitsOnly(int count, int score) ;

// Para actualizar el criterio de mayúsculas consecutivas.
void setConsecutiveUpper(int count, int score) ;

// Para actualizar el criterio de minúsculas consecutivas.
void setConsecutiveLower(int count, int score) ;

// Para actualizar el criterio de dígitos consecutivos.
void setConsecutiveDigits(int count, int score) ;
```

### Ejercicio 3 - Calcular la puntuación de los criterios y la puntuación total de la contraseña


El código que te proveemos contiene las funciones que computan el conteo para la mayoría de los criterios y cuyos nombres reflejan lo que hace y devuelve la función. Por ejemplo, `countUppercase`, devuelve el número de caracteres que son letras mayúsculas. Puedes encontrar una lista y descripción de las funciones en [este enlace](doc/es/html/index.html).

Tu tarea es utilizar expresiones matemáticas y estructuras condicionales para las puntuaciones de los criterios individuales y combinarlas para computar la puntuación total de fortaleza de una contraseña.




**Ejemplo 1:**

---

![ventanaCaba77o.png](images/ventanaCaba77o.png)

**Figura 2.** Ventana con el informe de la contraseña `caba77o`

---

**Ejemplo 2:**

---

![ventanaS1nf@nia!.png](images/ventanaS1nf@nia!.png)

**Figura 3.** Ventana con el informe de la contraseña `S1nf@nia!`

---

En el Ejemplo 2, el conteo de los **requisitos** es 5 porque `"S1nf@nia!"` cumple con el criterio de largo y también contiene mayúsculas, minúsculas, números y símbolos. Por lo tanto, la puntuación del número de requisitos es $$2 \cdot 5 =10$$.

En el código del proyecto vas a encontrar ejemplos de cómo calcular los primeros dos criterios positivos: el número de caracteres en la contraseña y el número de letras mayúsculas. Puedes compilar y ejecutar el ejemplo para que veas la interfaz funcionando con esos dos criterios. Parte de tu tarea es añadir el código para computar las puntuaciones de los demás criterios. Recuerda que debes ir acumulando el resultado de la puntuación total y hacer invocaciones para actualizar la interfaz gráfica.



### Ejercicio 4 - Determinar y desplegar la fortaleza de la contraseña

En la parte superior de la interfaz gráfica, el usuario ingresará la contraseña. Debajo aparece un *informe* que contiene los distintos criterios, el conteo para cada criterio, y la puntuación individual para los criterios. Este informe se va actualizando según el usuario va ingresando los caracteres de la contraseña.  La puntuación total será la suma de todas los puntos (sumas y restas) de los criterios individuales.

Basado en la puntuación total, el programa debe clasificar la fortaleza de la contraseña como sigue:

| Puntación total |  Fortaleza  |
|-----------------|-------------|
| [0,20)          | Bien débil  |
| [20,40)         | Débil       |
| [40,60)         | Buena       |
| [60,80)         | Fuerte      |
| [80,100]        | Bien fuerte |

El código provisto ya invoca la función `strengthDisplay` con la fortaleza calculada y la puntuación final para actualizar la clasificación y la barra que indica la fortaleza en la interfaz gráfica.

---

---

## Entregas

Utiliza "Entrega" en Moodle para entregar el archivo `readpassword.cpp` que contiene el código con el cómputo de las puntuaciones de los criterios individuales, la puntuación final, las invocaciones para actualizar la interfaz gráfica, la clasificación de la fortaleza, y su despliegue. Recuerda utilizar buenas prácticas de programación, incluye el nombre de los programadores y documenta tu programa.



---

---

## Referencias


[1] Passwordmeter, http://www.passwordmeter.com/

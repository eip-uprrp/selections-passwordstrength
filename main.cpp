#include <QApplication>
#include "mainwindow.h"
#include "qfile.h"

int main(int argc, char *argv[]){

    QApplication a(argc, argv);
    QFile File("://style_3");

    File.open(QFile::ReadOnly);
    QString StyleSheet = QLatin1String(File.readAll());

    qApp->setStyleSheet(StyleSheet);
    MainWindow w;
    w.setWindowTitle("Password Strength Lab");
    w.setStyleSheet("");
    w.show();

    return a.exec();
}

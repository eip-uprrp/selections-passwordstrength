#-------------------------------------------------
#
# Project created by QtCreator 2014-07-02T13:28:36
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets


TARGET = PasswordStrength
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    readpassword.cpp \
    psfunctions.cpp

HEADERS  += mainwindow.h \
    psfunctions.h

FORMS    += mainwindow.ui

RESOURCES += \
    images.qrc \
    style.qrc
